package net.dlm.music;

import org.apache.commons.configuration2.Configuration;
import org.junit.Assert;
import org.junit.Test;

import java.util.Iterator;
import java.util.List;


public class AllMelodiesTestCase {
    @Test
    public void testConfigurationReader() {
        AllMelodies app = new AllMelodies();
        Configuration config = app.initConfiguration("notes.properties");
        Assert.assertNotNull("notes.properties cannot be read.", config);
        List<String> noteList = config.getList(String.class, AllMelodies.NOTE_LIST_PROP_KEY);
        Assert.assertNotNull("note list missing", noteList);
        Assert.assertFalse("note list is empty", noteList.isEmpty());
    }
}
