package net.dlm.music;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.FileBasedConfiguration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.convert.DefaultListDelimiterHandler;
import org.apache.commons.configuration2.ex.ConfigurationException;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Generate all melodies over a single octave.
 */
public class AllMelodies {

    /**
     * Defaults to key of C.
     * Content should be overwritten by the notes from notes.properties
     */
    String noteNames[] = {"C","D","E","F","G","A","B","C"}; // default to the key of C
    Configuration config;

    static final String NOTE_LIST_PROP_KEY="melodies.note.scale.notes";
    static final String OUTPUT_FILE_NAME_PROP_KEY = "melodies.output.file.name";
    AllMelodies() {
    }

    public static void main(String[] args) {
        AllMelodies app = new AllMelodies();
        app.run();
    }

    private void run() {
        config = initConfiguration("notes.properties");
        List<String> noteList = config.getList(String.class, AllMelodies.NOTE_LIST_PROP_KEY);
        this.noteNames = new String[noteList.size()];
        this.noteNames = noteList.toArray(this.noteNames);

        // Using octal because it is a perfect fit for music scales with 8 notes per octave.
        String octalNumber = "000000";
        int xmin = Integer.parseInt(octalNumber, 8);

        octalNumber = "777777";
        int xmax = Integer.parseInt(octalNumber, 8);

        String outfile = config.get(String.class, AllMelodies.OUTPUT_FILE_NAME_PROP_KEY);

        try (FileWriter outFileWriter = new FileWriter(outfile);
             PrintWriter pw = new PrintWriter(outFileWriter);
        ) {
            String tmp = "";
            String melodie = "";
            for (int xhigh = xmin; xhigh <= xmax; xhigh++) {
                for (int xlow = xmin; xlow <= xmax; xlow++) {
                    String octalNumberLow = Integer.toString(xlow, 8);
                    String octalNumberHigh = Integer.toString(xhigh, 8);
                    tmp = pad6(octalNumberHigh) + pad6(octalNumberLow);
                    char[] noteNumbers = tmp.toCharArray();
                    melodie = convertToNotes(noteNumbers);
                    pw.print(tmp);
                    pw.print(" -");
                    pw.println(melodie);
                }
                pw.flush();
                System.out.println(melodie);
            }
            pw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    Configuration initConfiguration(String pName) {
        Configuration configLocal = null;
        Parameters params = new Parameters();
        FileBasedConfigurationBuilder<FileBasedConfiguration> builder =
                new FileBasedConfigurationBuilder<>(PropertiesConfiguration.class);

        builder.configure(params.properties().setListDelimiterHandler(new DefaultListDelimiterHandler(','))
                .setFileName(pName));

        try {
            configLocal = builder.getConfiguration();
        } catch(ConfigurationException cex) {
            cex.printStackTrace();
            System.exit(-1);
        } finally {

        }

        return configLocal;
    }

    /**
     * noteIndexes should be exactly 12 in length.
     * @param noteIndexes
     * @return
     */
    private String convertToNotes(char[] noteIndexes) {
        StringBuilder buf = new StringBuilder();
        for(int x = 0; x < 12; x++) {
            String noteName = noteNames[Integer.parseInt(String.valueOf(noteIndexes[x]))];
            buf.append(" ").append(noteName);
        }
        return buf.toString();
    }

    private String pad6(String input) {
        int requiredPad = 6 - input.length();
        String output = input;
        for(int x = 0; x < requiredPad; x++){
            output = "0" + output;
        }
        return output;
    }
}
